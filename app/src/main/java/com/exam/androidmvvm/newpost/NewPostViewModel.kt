package com.exam.androidmvvm.newpost

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.exam.androidmvvm.ScreenState
import com.exam.androidmvvm.data.database.AppDatabase
import com.exam.androidmvvm.model.UserPost

class NewPostViewModel(private val newPostInteractor: NewPostInteractor, private val context: Context) : ViewModel(),
    NewPostInteractor.OnRegiterFinishedListener {

    private val _newPostState: MutableLiveData<ScreenState<NewPostState>> = MutableLiveData()

    val newPostState: LiveData<ScreenState<NewPostState>>
        get() = _newPostState

    fun onPostClicked(caption: String, userId: Int,isPublic: Boolean,imagePath: String) {
        _newPostState.value = ScreenState.Loading
        newPostInteractor.submitPost(caption, userId, isPublic,imagePath,this)
    }

    override fun onCaptionEmpty()  {
        _newPostState.value = ScreenState.Render(NewPostState.EmptyCaption)
    }



    override fun onSuccess(caption: String, userId: Int,isPublic: Boolean,imagePath: String) {
        //insert to DB
        AppDatabase.getInstance(context).userPostDao().insert(UserPost(userId,caption,isPublic,imagePath))

        _newPostState.value = ScreenState.Render(NewPostState.Success)
    }
}

class NewPostViewModelFactory(private val newPostInteractor: NewPostInteractor, private val context: Context) :
    ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return NewPostViewModel(newPostInteractor,context) as T
    }
}