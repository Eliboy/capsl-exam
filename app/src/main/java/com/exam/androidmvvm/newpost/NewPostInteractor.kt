package com.exam.androidmvvm.newpost

import com.exam.androidmvvm.postDelayed

class NewPostInteractor {

    interface OnRegiterFinishedListener {
        fun onCaptionEmpty()
        fun onSuccess(caption: String, userId: Int,isPublic: Boolean,imagePath:String)
    }

    fun submitPost(caption: String, userId: Int,isPublic: Boolean,imagePath:String, listener: OnRegiterFinishedListener) {

        postDelayed(2000) {
            when {
                caption.isEmpty() -> listener.onCaptionEmpty()
                else -> listener.onSuccess(caption,userId,isPublic,imagePath)
            }
        }
    }
}