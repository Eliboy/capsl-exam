package com.exam.androidmvvm.newpost

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.RadioButton
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProviders
import com.exam.androidmvvm.R
import com.exam.androidmvvm.ScreenState
import com.exam.androidmvvm.main.MainActivity
import kotlinx.android.synthetic.main.activity_new_post.*


class NewPostActivity : AppCompatActivity() {


    private lateinit var viewModel : NewPostViewModel
    private var userId : Int = 0

    private var imagePath : String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_post)

        val bundle = intent.extras
        //Extract the data…
        userId = bundle.getInt("USER_ID")

        viewModel = ViewModelProviders.of(this,
            NewPostViewModelFactory(NewPostInteractor(),this@NewPostActivity)
        )[NewPostViewModel::class.java]

        viewModel.newPostState.observe(::getLifecycle, ::updateUI)
        button_save.setOnClickListener{ onSubmit()}
        image_view.setOnClickListener{openGallery()}




    }



    private fun updateUI(screenState: ScreenState<NewPostState>?) {
        when (screenState) {
            ScreenState.Loading -> progress.visibility = View.VISIBLE
            is ScreenState.Render -> processNewPostState(screenState.renderState)
        }
    }

    private fun processNewPostState(renderState: NewPostState) {
        progress.visibility = View.GONE
        when (renderState) {

            NewPostState.EmptyCaption -> caption.error = getString(R.string.caption_error)
            NewPostState.Success -> navigate()
        }
    }

    private fun navigate(){
        startActivity(Intent(this, MainActivity::class.java))
        finish()
    }

    private fun onSubmit() {


        val selectedId: Int = radioGroup.checkedRadioButtonId

        // find the radiobutton by returned id
        // find the radiobutton by returned id
        val radioButton = findViewById<View>(selectedId) as RadioButton
        var isPublic = true
        if( radioButton.getText().equals("Private")){
            isPublic = false
        }
        if (imagePath.equals("")){
            Toast.makeText(applicationContext,"Please select image",Toast.LENGTH_SHORT).show();
        }else{

            viewModel.onPostClicked(caption.text.toString(),userId, isPublic,imagePath)
        }
    }


    private fun openGallery(){
            //check runtime permission
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
                if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) ==
                    PackageManager.PERMISSION_DENIED){
                    //permission denied
                    val permissions = arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE);
                    //show popup to request runtime permission
                    requestPermissions(permissions, PERMISSION_CODE);
                }
                else{
                    //permission already granted
                    pickImageFromGallery();
                }
            }
            else{
                //system OS is < Marshmallow
                pickImageFromGallery();
            }

    }

    private fun pickImageFromGallery() {
        //Intent to pick image
        val intent = Intent(Intent.ACTION_PICK)
        intent.type = "image/*"
        startActivityForResult(intent, IMAGE_PICK_CODE)
    }


    companion object {
        //image pick code
        private val IMAGE_PICK_CODE = 1000;
        //Permission code
        private val PERMISSION_CODE = 1001;
    }




    //handle requested permission result
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when(requestCode){
            PERMISSION_CODE -> {
                if (grantResults.size >0 && grantResults[0] ==
                    PackageManager.PERMISSION_GRANTED){
                    //permission from popup granted
                    pickImageFromGallery()
                }
                else{
                    //permission from popup denied
                    Toast.makeText(this, "Permission denied", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    //handle result of picked image
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && requestCode == IMAGE_PICK_CODE){
            image_view.setImageURI(data?.data)
            var selectedImageUri : Uri? = data?.data
            imagePath = selectedImageUri.toString()
            Log.e("URL", "--   " + selectedImageUri.toString())


        }
    }
}
