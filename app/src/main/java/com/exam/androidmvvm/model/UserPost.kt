package com.exam.androidmvvm.model

import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity(tableName = "post_table")
data class UserPost(

    var userid: Int,
    var caption: String,
    var isPublic: Boolean,
    var imageLink: String
) {

    @PrimaryKey(autoGenerate = true)
    var postId: Int = 0

}