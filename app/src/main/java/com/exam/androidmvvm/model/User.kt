package com.exam.androidmvvm.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "user_table")
data class User(

    var username: String,
    var password: String,
    @PrimaryKey(autoGenerate = true) var id : Int

)