package com.exam.androidmvvm.data.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.exam.androidmvvm.model.User

@Dao
interface UserDao {

    @Insert
    fun insert(user: User)

    @Query("DELETE FROM user_table")
    fun deleteAllUsers()

    @Query("SELECT * FROM user_table WHERE username = :username AND password = :password ")
    fun getUser(username:String,password:String): User

    @Query("SELECT * FROM user_table WHERE id = :userId")
    fun getUserById(userId:Int) : User



}