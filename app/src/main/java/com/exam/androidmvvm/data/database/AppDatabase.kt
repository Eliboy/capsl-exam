package com.exam.androidmvvm.data.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.exam.androidmvvm.data.dao.UserDao
import com.exam.androidmvvm.data.dao.UserPostDao
import com.exam.androidmvvm.model.User
import com.exam.androidmvvm.model.UserPost


@Database(entities = [(User::class),(UserPost::class)], version = 3)
abstract class AppDatabase : RoomDatabase() {

    abstract fun userDao(): UserDao
    abstract fun userPostDao(): UserPostDao

    companion object {
        private var instance: AppDatabase? = null

        @Synchronized
        fun getInstance(context: Context): AppDatabase {
            if (instance == null) {
                instance = Room.databaseBuilder(context.applicationContext, AppDatabase::class.java, "TEST_DB")
                    .fallbackToDestructiveMigration()
                    .allowMainThreadQueries()
                    .build()
            }

            return instance!!
        }
    }



}






