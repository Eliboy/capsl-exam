package com.exam.androidmvvm.data.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.exam.androidmvvm.model.UserPost

@Dao
interface UserPostDao {

    @Insert
    fun insert(userPost: UserPost)

    @Query("DELETE FROM post_table")
    fun deleteAllPost()

    @Query("SELECT * FROM post_table WHERE userid = :userid AND isPublic = :isPublic")
    fun getUserPost(userid:Int,isPublic:Boolean):  LiveData<List<UserPost>>

    @Query("SELECT * FROM post_table WHERE isPublic = 1")
    fun getAllPublicPost():  LiveData<List<UserPost>>

    @Query("SELECT * FROM post_table")
    fun getMyPost():  LiveData<List<UserPost>>
}