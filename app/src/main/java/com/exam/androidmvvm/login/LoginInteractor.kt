package com.exam.androidmvvm.login

import androidx.lifecycle.LiveData
import com.exam.androidmvvm.data.dao.UserDao
import com.exam.androidmvvm.data.dao.UserPostDao
import com.exam.androidmvvm.data.database.AppDatabase
import com.exam.androidmvvm.model.User
import com.exam.androidmvvm.postDelayed
import kotlinx.coroutines.*
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.Dispatchers.Main

class LoginInteractor() {

    interface OnLoginFinishedListener {
        fun onUsernameError()
        fun onPasswordError()
        fun onSuccess(username: String, password: String)
    }

    fun login(username: String, password: String, listener: OnLoginFinishedListener) {


        postDelayed(2000) {
            when {
                username.isEmpty() -> listener.onUsernameError()
                password.isEmpty() -> listener.onPasswordError()
                else -> listener.onSuccess(username,password)
            }
        }
    }


//    var job : CompletableJob? = null
//
//    fun getUser(userId:Int) : LiveData<User>{
//        job = Job()
//        return object : LiveData<User>(){
//            override fun onActive() {
//                super.onActive()
//                job?.let { theJob ->
//                    CoroutineScope(IO + theJob).launch {
//                        val user =  userDao.getUserById(userId)
//                        withContext(Main){
//                            value = user
//                            theJob.complete()
//                        }
//                    }
//                }
//            }
//        }
//    }
//
//    fun cancelJobs(){
//        job?.cancel()
//    }
}