package com.exam.androidmvvm.login

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.exam.androidmvvm.R
import com.exam.androidmvvm.ScreenState
import com.exam.androidmvvm.SharedPreference
import com.exam.androidmvvm.main.MainActivity
import com.exam.androidmvvm.model.User
import com.exam.androidmvvm.registration.RegistrationActivity
import kotlinx.android.synthetic.main.activity_login.*


class LoginActivity : AppCompatActivity() {

    private lateinit var viewModel : LoginViewModel
    private lateinit var userDetails: User

    lateinit var sharedPreference: SharedPreference

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        sharedPreference  =SharedPreference(this)

        viewModel = ViewModelProviders.of(
            this,
            LoginViewModelFactory(LoginInteractor(),this@LoginActivity)
        )[LoginViewModel::class.java]

        viewModel.loginState.observe(::getLifecycle, ::updateUI)

        viewModel.userDetails.observe(this, Observer {user ->
            userDetails = user
        })

        button.setOnClickListener { onLoginClicked() }

        signup.setOnClickListener { onSignUpClicked() }

    }

    private fun updateUI(screenState: ScreenState<LoginState>?) {
        when (screenState) {
            ScreenState.Loading -> progress.visibility = View.VISIBLE
            is ScreenState.Render -> processLoginState(screenState.renderState)
        }
    }

    private fun processLoginState(renderState: LoginState ) {
        progress.visibility = View.GONE
        when (renderState) {

            LoginState.WrongUserName -> username.error = getString(R.string.username_error)
            LoginState.WrongPassword -> password.error = getString(R.string.password_error)
            LoginState.NoUser -> password.error = getString(R.string.no_user)
            LoginState.Success -> navigate()

        }
    }

    private fun navigate(){
        println("Debug: ${userDetails}")
        Log.e("ELI", "DATA   ___ " + userDetails)


        sharedPreference.save("userid",userDetails.id)

        startActivity(Intent(this, MainActivity::class.java))
        finish()
    }
    private fun onLoginClicked() {

        viewModel.onLoginClicked(username.text.toString(), password.text.toString())
    }

    private fun onSignUpClicked() {

        startActivity(Intent(this, RegistrationActivity::class.java))
    }

}
