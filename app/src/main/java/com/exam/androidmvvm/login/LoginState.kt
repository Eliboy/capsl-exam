package com.exam.androidmvvm.login

enum class LoginState {
    Success, WrongUserName, WrongPassword, NoUser
}