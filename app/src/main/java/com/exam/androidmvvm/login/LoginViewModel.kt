package com.exam.androidmvvm.login

import android.content.Context
import androidx.lifecycle.*
import com.exam.androidmvvm.ScreenState
import com.exam.androidmvvm.data.database.AppDatabase
import com.exam.androidmvvm.model.User
import com.exam.androidmvvm.model.UserPost

class LoginViewModel(private val loginInteractor: LoginInteractor, private val context: Context) : ViewModel(),
    LoginInteractor.OnLoginFinishedListener {


    private val _user: MutableLiveData<User> = MutableLiveData()

    val userDetails: LiveData<User>
        get() =_user


    private val _loginState: MutableLiveData<ScreenState<LoginState>> = MutableLiveData()
    val loginState: LiveData<ScreenState<LoginState>>
        get() = _loginState

    fun onLoginClicked(username: String, password: String) {
        _loginState.value = ScreenState.Loading
        loginInteractor.login(username, password, this)
    }

    override fun onUsernameError() {
        _loginState.value = ScreenState.Render(LoginState.WrongUserName)
    }

    override fun onPasswordError() {
        _loginState.value = ScreenState.Render(LoginState.WrongPassword)
    }

    override fun onSuccess(username: String, password: String) {
        var user:User =AppDatabase.getInstance(context).userDao().getUser(username,password)

//        Log.i("USER", "name: " + user.username)
        if (user!=null) {
            _user.value =user
                _loginState.value = ScreenState.Render(LoginState.Success)
        }else{
            _loginState.value = ScreenState.Render(LoginState.NoUser)
        }
    }



}

class LoginViewModelFactory(private val loginInteractor: LoginInteractor, private val context: Context) :
    ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return LoginViewModel(loginInteractor,context) as T
    }
}

