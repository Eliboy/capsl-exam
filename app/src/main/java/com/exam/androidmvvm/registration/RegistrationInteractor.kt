package com.exam.androidmvvm.registration

import com.exam.androidmvvm.postDelayed

class RegistrationInteractor {

    interface OnRegiterFinishedListener {
        fun onUsernameError()
        fun onPasswordError()
        fun onSuccess(username: String, password: String)
    }

    fun register(username: String, password: String, listener: OnRegiterFinishedListener) {

        postDelayed(2000) {
            when {
                username.isEmpty() -> listener.onUsernameError()
                password.isEmpty() -> listener.onPasswordError()
                else -> listener.onSuccess(username,password)
            }
        }
    }
}