package com.exam.androidmvvm.registration

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModelProviders
import com.exam.androidmvvm.R
import com.exam.androidmvvm.ScreenState
import com.exam.androidmvvm.login.LoginActivity
import kotlinx.android.synthetic.main.activity_registration.*
import kotlinx.android.synthetic.main.activity_registration.button
import kotlinx.android.synthetic.main.activity_registration.password
import kotlinx.android.synthetic.main.activity_registration.username

class RegistrationActivity : AppCompatActivity() {

    private lateinit var viewModel : RegistrationViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registration)


        viewModel = ViewModelProviders.of(this,
        RegistrationViewModelFactory(RegistrationInteractor(),this@RegistrationActivity)
        )[RegistrationViewModel::class.java]

        viewModel.registrationState.observe(::getLifecycle, ::updateUI)
        button.setOnClickListener { onRegisterClicked() }
    }

    private fun updateUI(screenState: ScreenState<RegistrationState>?) {
        when (screenState) {
            ScreenState.Loading -> progress.visibility = View.VISIBLE
            is ScreenState.Render -> processRegistrationState(screenState.renderState)
        }
    }

    private fun processRegistrationState(renderState: RegistrationState) {
        progress.visibility = View.GONE
        when (renderState) {
            RegistrationState.Success -> navigate()
            RegistrationState.EmptyUserName -> username.error = getString(R.string.username_error)
            RegistrationState.EmptyPassword -> password.error = getString(R.string.password_error)
        }
    }
    private fun navigate(){
        startActivity(Intent(this, LoginActivity::class.java))
        finish()
    }

    private fun onRegisterClicked() {
        viewModel.onRegisterClicked(username.text.toString(), password.text.toString())
    }
}
