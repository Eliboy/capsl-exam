package com.exam.androidmvvm.registration

enum class RegistrationState {
    Success, EmptyUserName, EmptyPassword
}