package com.exam.androidmvvm.registration

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.exam.androidmvvm.ScreenState
import com.exam.androidmvvm.data.database.AppDatabase
import com.exam.androidmvvm.model.User

class RegistrationViewModel(private val registrationInteractor: RegistrationInteractor, private val context: Context) : ViewModel(),
    RegistrationInteractor.OnRegiterFinishedListener {

    private val _registrationState: MutableLiveData<ScreenState<RegistrationState>> = MutableLiveData()

    val registrationState: LiveData<ScreenState<RegistrationState>>
        get() = _registrationState

    fun onRegisterClicked(username: String, password: String) {
        _registrationState.value = ScreenState.Loading
        registrationInteractor.register(username, password, this)
    }

    override fun onUsernameError() {
        _registrationState.value = ScreenState.Render(RegistrationState.EmptyUserName)
    }

    override fun onPasswordError() {
        _registrationState.value = ScreenState.Render(RegistrationState.EmptyPassword)
    }

    override fun onSuccess(username: String, password: String) {
        //insert to DB
        AppDatabase.getInstance(context).userDao().insert(User(username,password,0))

        _registrationState.value = ScreenState.Render(RegistrationState.Success)
    }
}

class RegistrationViewModelFactory(private val registrationInteractor: RegistrationInteractor, private val context: Context) :
    ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return RegistrationViewModel(registrationInteractor,context) as T
    }
}