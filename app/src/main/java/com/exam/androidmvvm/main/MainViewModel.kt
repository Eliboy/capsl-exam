package com.exam.androidmvvm.main

import android.app.Application
import androidx.lifecycle.*
import com.exam.androidmvvm.ScreenState
import com.exam.androidmvvm.data.database.AppDatabase
import com.exam.androidmvvm.login.LoginInteractor
import com.exam.androidmvvm.model.User
import com.exam.androidmvvm.model.UserPost
import kotlinx.coroutines.launch

class MainViewModel(application: Application) : AndroidViewModel(application) {

    // The ViewModel maintains a reference to the repository to get data.
    private val itemInteractor: FindItemsInteractor
    // LiveData gives us updated words when they change.
    val allPost: LiveData<List<UserPost>>


    init {
        // Gets reference to WordDao from WordRoomDatabase to construct
        // the correct WordRepository.
        val postDao = AppDatabase.getInstance(application).userPostDao()
        itemInteractor = FindItemsInteractor(postDao)
        allPost = itemInteractor.allPost



    }

    /**
     * The implementation of insert() in the database is completely hidden from the UI.
     * Room ensures that you're not doing any long running operations on the mainthread, blocking
     * the UI, so we don't need to handle changing Dispatchers.
     * ViewModels have a coroutine scope based on their lifecycle called viewModelScope which we
     * can use here.
     */
    fun insert(userPost: UserPost) = viewModelScope.launch {
        itemInteractor.insert(userPost)
    }
}