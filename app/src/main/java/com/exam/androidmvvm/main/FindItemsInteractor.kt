package com.exam.androidmvvm.main

import androidx.lifecycle.LiveData
import com.exam.androidmvvm.data.dao.UserPostDao
import com.exam.androidmvvm.model.UserPost
import com.exam.androidmvvm.postDelayed

class FindItemsInteractor(private val userPostDao: UserPostDao) {

    // Room executes all queries on a separate thread.
    // Observed LiveData will notify the observer when the data has changed.
    val allPost: LiveData<List<UserPost>> = userPostDao.getAllPublicPost()

    suspend fun insert(userPost: UserPost) {
        userPostDao.insert(userPost)
    }
}