package com.exam.androidmvvm.main

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.exam.androidmvvm.R
import com.exam.androidmvvm.SharedPreference
import com.exam.androidmvvm.newpost.NewPostActivity
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    private lateinit var viewModel: MainViewModel
    private var userId : Int = 0
    lateinit var sharedPreference: SharedPreference

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        sharedPreference = SharedPreference(this)

        if (sharedPreference.getValueInt("userid")!=null) {
            userId = sharedPreference.getValueInt("userid")!!
        }



        val adapter = MainAdapter(this)
        list.adapter = adapter
        list.layoutManager = LinearLayoutManager(this)

        // Get a new or existing ViewModel from the ViewModelProvider.
        viewModel = ViewModelProvider(this).get(MainViewModel::class.java)



        viewModel.allPost.observe(this, Observer { userPost ->
            // Update the cached copy of the words in the adapter.
            userPost?.let { adapter.setPost(userPost) }
        })

        addPost.setOnClickListener { addPost() }
    }


    private fun addPost() {
        val i = Intent(this, NewPostActivity::class.java)
        //Create the bundle
        val bundle = Bundle()
        bundle.putInt("USER_ID", userId)
        //Add the bundle to the intent
        i.putExtras(bundle)
        startActivity(i)
        finish()

    }

}
