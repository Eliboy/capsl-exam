package com.exam.androidmvvm.main

import android.content.Context
import android.net.Uri
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.exam.androidmvvm.R
import com.exam.androidmvvm.model.UserPost


class MainAdapter internal constructor(
    context: Context
) : RecyclerView.Adapter<MainAdapter.ViewHolder>() {

    private val inflater: LayoutInflater = LayoutInflater.from(context)
    private var userPost = emptyList<UserPost>() // Cached copy of words

    private val context: Context = context

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val wordItemView: TextView = itemView.findViewById(R.id.caption)
        val thumbnail: ImageView = itemView.findViewById(R.id.thumbnail)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView = inflater.inflate(R.layout.recyclerview_item, parent, false)
        return ViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val current = userPost[position]
        holder.wordItemView.text = current.caption

        val uri: Uri = Uri.parse(current.imageLink)
        Log.e("URLLLL", "----" + uri.toString())
        Glide.with(context)
            .load(Uri.parse(current.imageLink))
            .into(holder.thumbnail)




    }

    internal fun setPost(userPost: List<UserPost>) {
        this.userPost = userPost
        notifyDataSetChanged()
    }

    override fun getItemCount() = userPost.size
}